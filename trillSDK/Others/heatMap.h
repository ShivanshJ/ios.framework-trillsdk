//
//  heatMap.h
//  trillSDK
//
//  Created by Mac Mini on 12/4/17.
//  Copyright © 2017 TrillBit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface heatMap : NSObject
{
    NSString *_deviceID;
    NSString *_deviceModel;
    NSMutableArray *_zoneID;
    NSMutableArray *_trigger_chunks;
}
@property(strong, nonatomic, readwrite) NSString *deviceID;
@property(strong, nonatomic, readwrite) NSString *deviceModel;
@property(strong, nonatomic, readwrite) NSMutableArray *zoneID;
@property(strong, nonatomic, readwrite) NSMutableArray *trigger_chunks;
+ (heatMap *) sharedInstance;

-(id) init: (NSString *)devID deviceModel:(NSString *)devModel;

//----TO SAVE THE TONE RECEIVED, BE IT ZONE,INTERACTION
-(void )saveUserData: (int)code : (NSMutableArray *) power_spectrum2D;

//----BASICALLY A TIMER WHICH RUNS FOR 1 MIN AND ON COMPLETION UPLOADS SAVED DATA BACK TO SERVER
-(void)checkCounter;

//----FINAL METHOD TO BE CALLED. THIS MAKES HTTP REQUEST AND SAVES DATA OVER SERVER
-(void )uploadUserData: (void (^)(void) )completionHandler ;


@end
