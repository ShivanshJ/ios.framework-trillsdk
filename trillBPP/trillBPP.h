//
//  trillBPP.h
//  trillBPP
//
//  Created by Shivansh on 11/13/17.
//  Copyright © 2017 TrillBit. All rights reserved.
//


//! Project version number for trillBPP.
FOUNDATION_EXPORT double trillBPPVersionNumber;

//! Project version string for trillBPP.
FOUNDATION_EXPORT const unsigned char trillBPPVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <trillBPP/PublicHeader.h>

#import <trillBPP/finddata.h>
#import <trillBPP/vec.h>
#import <trillBPP/trigger.h>
