/*
 * miscfunc.h
 *
 *  Created on: 29-Sep-2017
 *      Author: shivujagga
 */

#ifndef TRILLBPP_MISCFUNC_H_
#define TRILLBPP_MISCFUNC_H_

#include <trillBPP/vec.h>

namespace trill{

//Conjugate of complex number
	cvec conj(const cvec &x);
	cmat conj(const cmat &x);

//Absolute value of cvec
	vec abs(const cvec &data);

//! The nearest larger integer
	inline int ceil_i(double x) { return static_cast<int>(std::ceil(x)); }

//levels2bit
	inline int levels2bits(int n) {
	  it_assert(n >= 0, "int2bits(): Improper argument value");
	  if (n == 0) return 1;
	  int b = 0;
	  while (n) { n >>= 1;
		++b;
	  }
	  return b;
	}

	/*!
	\relatesalso Vec
	\brief Converts a Vec<T> to cvec
	*/
	template <class T>
	cvec to_cvec(const Vec<T> &v) {
	  cvec temp(v.length());
	  for (int i = 0; i < v.length(); ++i) {
	    temp(i) = std::complex<double>(static_cast<double>(v(i)), 0.0);
	  }
	  return temp;
	}
	//! \cond
	template<> inline
	cvec to_cvec(const cvec& v)
	{
	  return v;
	}
	//! \endcond


//Getting real part of cvec as vec
	vec real(const cvec &data);

//Zero padding
	//! Zero-pad a vector to size n
	template<class T>
	Vec<T> zero_pad(const Vec<T> &v, int n)
	{
	  it_assert(n >= v.size(), "zero_pad() cannot shrink the vector!");
	  Vec<T> v2(n);
	  v2.set_subvector(0, v);
	  if (n > v.size())
	    v2.set_subvector(v.size(), n - 1, T(0));

	  return v2;
	}


} //namespace trill

#endif /* TRILLBPP_MISCFUNC_H_ */
